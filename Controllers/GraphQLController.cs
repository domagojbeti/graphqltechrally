﻿using graphqltechrally.Database;
using GraphQL.Types;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using graphqltechrally.GraphQL;
using GraphQL;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace graphqltechrally.Controllers
{
	[ApiController]
	[Route("graphql")]
	public class GraphQLController : ControllerBase
	{
		private readonly ApplicationDbContext _context;

		public GraphQLController(ApplicationDbContext context) => _context = context;

		public async Task<IActionResult> Post([FromBody] System.Text.Json.JsonElement rawQuery)
		{
			var rawJson = rawQuery.ToString();
			var query = JsonConvert.DeserializeObject<GraphQLQuery>(rawJson);

			var schema = new Schema
			{
				Query = new ReservationQuery(_context),
				Mutation = new ReservationMutation(_context)
			};

			var result = await new DocumentExecuter().ExecuteAsync(_ =>
			{
				_.Schema = schema;
				_.Query = query.Query;
				_.OperationName = query.OperationName;
				_.Inputs = query.GetInputs();
			});

			if (result.Errors?.Count > 0)
			{
				return BadRequest(string.Join(",", result.Errors.Select(x => x.Message)));
			}

			return Ok(result.Data);
		}
    }
}
