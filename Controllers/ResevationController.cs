﻿using System.Collections.Generic;
using System.Linq;
using graphqltechrally.Database;
using graphqltechrally.Database.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace graphqltechrally.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReservationController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ReservationController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<Reservation> Get()
        {
            return _context.Reservations.Include(x => x.Customer).ToArray();
        }
    }
}
