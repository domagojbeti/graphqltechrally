﻿using graphqltechrally.Database.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace graphqltechrally.Database
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

		public DbSet<Reservation> Reservations { get; set; }

		public DbSet<ReservationItem> ReservationItems { get; set; }

		public DbSet<ReservationItemDetail> ReservationItemDetails { get; set; }

		public DbSet<Customer> Customers { get; set; }

		public DbSet<Service> Services { get; set; }

		public void Seed()
		{
			var customer = Customers.Add(new Customer
			{
				Name = "Lemax"
			});

			var service = Services.Add(new Service
			{
				Name = "Bed and breakfast"
			});

			SaveChanges();

			var reservation1 = Reservations.Add(new Reservation
			{
				CustomerId = customer.Entity.Id,
				DateCreated = DateTime.UtcNow
			});

			var reservation2 = Reservations.Add(new Reservation
			{
				CustomerId = customer.Entity.Id,
				DateCreated = DateTime.UtcNow
			});

			SaveChanges();

			var reservationItem1 = ReservationItems.Add(new ReservationItem
			{
				ReservationId = reservation1.Entity.Id,
				StartDate = new DateTime(2020, 12, 1),
				EndDate = new DateTime(2020, 12, 7)
			});

			var reservationItem2 = ReservationItems.Add(new ReservationItem
			{
				ReservationId = reservation2.Entity.Id,
				StartDate = new DateTime(2020, 12, 1),
				EndDate = new DateTime(2020, 12, 7)
			});

			SaveChanges();

			var reservationItemDetail1 = ReservationItemDetails.Add(new ReservationItemDetail
			{
				Price = 100M,
				ReservationItemId = reservationItem1.Entity.Id,
				ServiceId = service.Entity.Id
			});

			var reservationItemDetail2 = ReservationItemDetails.Add(new ReservationItemDetail
			{
				Price = 100M,
				ReservationItemId = reservationItem2.Entity.Id,
				ServiceId = service.Entity.Id
			});


			SaveChanges();
		}
	}
}
