﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace graphqltechrally.Database.Models
{
	public class Reservation
	{
		public int Id { get; set; }
		public DateTime DateCreated { get; set; }
		public int? CustomerId { get; set; }
		public virtual Customer Customer { get; set; }
		public virtual ICollection<ReservationItem> ReservationItems { get; set; }
	}
}
