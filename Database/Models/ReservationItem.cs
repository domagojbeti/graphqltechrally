﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace graphqltechrally.Database.Models
{
	public class ReservationItem
	{
		public int Id { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
		public int ReservationId { get; set; }
		public virtual Reservation Reservation { get; set; }
		public virtual ICollection<ReservationItemDetail> ReservationItemDetails {get;set;}
	}
}
