﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace graphqltechrally.Database.Models
{
	public class ReservationItemDetail
	{
		public int Id { get; set; }
		public int ReservationItemId { get; set; }
		public virtual ReservationItem ReservationItem { get; set; }
		public int ServiceId { get; set; }
		public virtual Service Service {get;set; }
		public decimal Price { get; set; }
	}
}
