﻿using GraphQL.Types;
using graphqltechrally.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace graphqltechrally.GraphQL
{
	public class CustomerType : ObjectGraphType<Customer>
	{
		public CustomerType()
		{
			Name = "Customer";
			Field(x => x.Id, type: typeof(IdGraphType)).Description("The Id of the Customer");
			Field(x => x.Name).Description("The name of the Customer");
		}
	}
}
