﻿using GraphQL.Types;

namespace graphqltechrally.GraphQL
{
	public class ReservationInputType : InputObjectGraphType
	{
		public ReservationInputType()
		{
			Name = "ReservationInput";
			Field<NonNullGraphType<IdGraphType>>("Id");
			Field<DateTimeGraphType>("DateCreated");
		}
	}
}
