﻿using GraphQL.Types;
using graphqltechrally.Database.Models;

namespace graphqltechrally.GraphQL
{
	public class ReservationItemDetailType : ObjectGraphType<ReservationItemDetail>
	{
		public ReservationItemDetailType()
		{
			Name = "ReservationItemDetail";
			Field(x => x.Id, type: typeof(IdGraphType))
				.Description("The Id of the ReservationItemDetail");
			Field(x => x.Price)
				.Description("The price of the ReservationItemDetail");
			Field(x => x.ReservationItem, type: typeof(ReservationItemType))
				.Description("The reservation item of the ReservationItemDetail");
			Field(x => x.Service, type: typeof(ServiceType))
				.Description("The service of the ReservationItemDetail");
		}
	}
}
