﻿using GraphQL.Types;
using graphqltechrally.Database.Models;

namespace graphqltechrally.GraphQL
{
	public class ReservationItemType : ObjectGraphType<ReservationItem>
	{
		public ReservationItemType()
		{
			Name = "ReservationItem";
			Field(x => x.Id, type: typeof(IdGraphType))
				.Description("The Id of the ReservationItem");
			Field(x => x.StartDate)
				.Description("The start date of the ReservationItem");
			Field(x => x.EndDate)
				.Description("The end date of the ReservationItem");
			Field(x => x.Reservation, type: typeof(ReservationType))
				.Description("The reservation of the ReservationItem");
			Field(x => x.ReservationItemDetails, type: typeof(ListGraphType<ReservationItemDetailType>))
				.Description("The reservation item details of the ReservationItem");
		}
	}
}
