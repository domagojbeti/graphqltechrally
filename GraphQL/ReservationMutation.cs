﻿using GraphQL;
using GraphQL.Types;
using graphqltechrally.Database;
using graphqltechrally.Database.Models;

namespace graphqltechrally.GraphQL
{
	public class ReservationMutation : ObjectGraphType
	{
		public ReservationMutation(ApplicationDbContext dbContext)
		{
            Field<ReservationType>(
			  "createReservation",
			  arguments: new QueryArguments(
				new QueryArgument<NonNullGraphType<ReservationInputType>> { Name = "reservation" }
			  ),
			  resolve: context =>
			  {
				  var reservation = context.GetArgument<Reservation>("reservation");
				  dbContext.Reservations.Add(reservation);
				  dbContext.SaveChanges();
				  return reservation;
			  });

			Field<ReservationType>(
			  "updateReservation",
			  arguments: new QueryArguments(
				new QueryArgument<NonNullGraphType<ReservationInputType>> { Name = "reservation" }
			  ),
			  resolve: context =>
			  {
				  var reservation = context.GetArgument<Reservation>("reservation");
				  dbContext.Reservations.Update(reservation);
				  dbContext.SaveChanges();
				  return reservation;
			  });
		}
	}
}
