﻿using GraphQL;
using GraphQL.Language.AST;
using GraphQL.Types;
using graphqltechrally.Database;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace graphqltechrally.GraphQL
{
	public class ReservationQuery : ObjectGraphType
	{
		public ReservationQuery(ApplicationDbContext dbContext)
		{
			Field<ReservationType>(
				"Reservation",
				arguments: new QueryArguments(
					new QueryArgument<IdGraphType>
					{
						Name = "id",
						Description = "The Id of the Reservation"
					}
				),
				resolve: context =>
				{
					var id = context.GetArgument<int>("id");

					var reservation = dbContext.Reservations
						.Include("Customer")
						.Include("ReservationItems.ReservationItemDetails.Service")
						.FirstOrDefault(x => x.Id == id);

					return reservation;
				});

			Field<ListGraphType<ReservationType>>(
				"Reservations",
				resolve: context =>
				{
					var stuff = dbContext.Reservations.ToList();
					var reservations = dbContext.Reservations
						.Include("Customer")
						.Include("ReservationItems.ReservationItemDetails.Service");

					return reservations;
				});
		}
	}
}
