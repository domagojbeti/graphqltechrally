﻿using GraphQL.Types;
using graphqltechrally.Database.Models;

namespace graphqltechrally.GraphQL
{
	public class ReservationType : ObjectGraphType<Reservation>
	{
		public ReservationType()
		{
			Name = "Reservation";
			Field(x => x.Id, type: typeof(IdGraphType)).Description("The Id of the Reservation.");
			Field(x => x.DateCreated).Description("The created date of the Reservation");
			Field(x => x.ReservationItems, type: typeof(ListGraphType<ReservationItemType>)).Description("Reservation's items");
			Field(x => x.Customer, type: typeof(CustomerType)).Description("The customer of the Reservation");
		}
	}
}
