﻿using GraphQL.Types;
using graphqltechrally.Database.Models;

namespace graphqltechrally.GraphQL
{
	public class ServiceType : ObjectGraphType<Service>
	{
		public ServiceType()
		{
			Name = "Service";
			Field(x => x.Id, type: typeof(IdGraphType))
				.Description("The Id of the Service");
			Field(x => x.Name)
				.Description("The name of the service");
		}
	}
}
